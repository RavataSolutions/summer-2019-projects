%data 7.23.19

%range frequency sweep (27,46):546,(548,567):1067

data1 = T1;
wellNum1 = 8;
data2 = T1;
wellNum2 = 9;

impedanceCol = 8;
phaseCol = 5;
imagCol = 4;
index = 1;
count = 0;
f = figure();
sgtitle('Buffer imaginary:  File: T1   Well: 8,9');
for(freq = [4000:4000:104000])
    
    dataAvg1(index) = mean(data1(27+20*count:46+20*count,imagCol + (6*(wellNum1-1))));
    dataAvg2(index) = mean(data2(27+20*count:46+20*count,imagCol + (6*(wellNum2-1))));
    
    dataStd1(index) = std(data1(27+20*count:46+20*count,imagCol + (6*(wellNum1-1))));
    dataStd2(index) = std(data2(27+20*count:46+20*count,imagCol + (6*(wellNum2-1))));
    %plotdata(index) = abs(dataAvg1(index) - dataAvg2(index));
    
    index = index + 1;
    count = count + 1;
end
errorbar([4000:4000:104000],dataAvg1,dataStd1,dataStd1);
%plot([4000:4000:104000],plotdata);
hold on;
errorbar([4000:4000:104000],dataAvg2,dataStd2,dataStd2);
xlabel('Frequency');
ylabel('Imaginary');
legend('data 1','data 1');
hold off;