Name of file format: Device#_Date_Experiment.txt

Device: D2
Title: System Phase All wells
Experiment Completed by: James Nelson and Gurkern Sufi
Date: 7.23.19
Location: Inventopia
Arduino Code used: well_monitoring_putty_James_phase_all_wells.ino

Number of experiments:1
Rcal/Step size: 10k
Frequency sweep: yes 4-104k, 4k step
Wells 1-22,System Phase
csv file: printing system phase (calibration) and then 2 independent frequency sweeps
------------------------------------------------------------------
Trial 1
Chip:E2
Buffer:Opti-mem
Observations: well 1 bubble, well 3 embryo
Notes:Performed calibration with buffer and then ran frequency sweep with cells
	Ignore data on wells 23-27
--------------------------------------------------------------------
Trial 2
Chip:E2
Buffer:Opti-mem
Observations:wells with cell 2,3,4,5,6,7,8,9
		2nd frequency sweep is junk data
--------------------------------------------------------------------
Trial 3
Chip:E4
Buffer:Opti-mem
1st frequency sweep buffer
2nd freqeuncy sweep cell
Observations:during calibration and 1st frequency sweep, wells 5,6 had a cell
	wells with cell
		1- cell not touching electrode
		2- cell went through electrode, stuck in small gap
		3,4,5 - 50% cell in between electrode
		6-bubble, obstruction in microfluidic that made it difficult
			for other cells to pass
		7,8,9 - cell
		10,11 - possibly a cell
		15-cell
	