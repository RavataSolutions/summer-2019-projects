
data = c1_1conc;

    time = data(1009:1258,1);
    impedance(28,250) = 0;
    phase(28,250) = 0;
for(wellNum = 1:28)
    impedance(wellNum,:) = data(758:1007,7 + (6*(wellNum-1))); %freq = 98khz
    phase(wellNum,:) = data(758:1007,4 + (6*(wellNum-1))); %(1009:1258)
end

f = figure();
sgtitle('c1-1conc 76khz');
subplot(1,2,1);%impedance magnitude
for(wellNum = 1:2)
   hold on;
   plot(time,impedance(wellNum,:));
end
xlabel('time');
ylabel('Magnitude');
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28');
hold off


subplot(1,2,2);%phase
for(wellNum = 1:2)
   hold on;
   plot(time,rad2deg(phase(wellNum,:)));
end
xlabel('time');
ylabel('Phase');
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28');
hold off


