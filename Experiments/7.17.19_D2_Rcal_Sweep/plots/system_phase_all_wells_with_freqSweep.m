data = T3;
index = 1;
count = 0;

impedanceCol = 8;
phaseCol = 5;
imagCol = 4;
f = figure();
sgtitle('T2 Frequency Sweep - Imaginary');
%range frequency sweep (27,46):546,(548,567):1067
for(wellNum =[1:22,28])
    
    for(freq = 4000:4000:104000)
        dataAvg(index) = mean(data(27+20*count:46+20*count,phaseCol + (6*(wellNum-1))));
        %phaseAvg(index) = mean(data(26+20*count:45+20*count,phaseCol + (6*(wellNum-1))));
        dataStd(index) = std(data(27+20*count:46+20*count,phaseCol + (6*(wellNum-1))));
        %phaseStd(index) = std(data(27+20*count:45+20*count,phaseCol + (6*(wellNum-1))));
        index = index+1;
        count = count+1;
    end
    errorbar([4000:4000:104000],rad2deg(dataAvg),dataStd,dataStd);
    %loglog(dataAv)
    hold on
    index = 1;
    count = 0;
end
xlabel('Frequency');
ylabel('Phase');
legend('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','28');

hold off