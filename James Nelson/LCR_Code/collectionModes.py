import device
import time
import csv
import numpy as np
import struct

def collectData(inst_handler):
    lcrData = dict()
    WAIT_TIME_SEC=0.25
    WAIT_TIME_SEC_LONG = 0.25
    '''
        ZTD - impedance mag,phase degrees
        GB - Conductance, Susceptance
        RX - Resistance,Reactance
    '''
    #Collect ZTD
    device.setImpedance(inst_handler,"75K",True,"ZTD")
    time.sleep(WAIT_TIME_SEC_LONG)
    values = inst_handler.query_ascii_values('FETC?',
              container=np.array)
    time.sleep(WAIT_TIME_SEC)
    lcrData['Z'] = values[0]
    lcrData['P'] = values[1]

    #Collect GB
    device.setImpedance(inst_handler,"75K",True,"GB")
    time.sleep(WAIT_TIME_SEC_LONG)
    values = inst_handler.query_ascii_values('FETC?',
              container=np.array)
    time.sleep(WAIT_TIME_SEC)
    lcrData['G'] = values[0]
    lcrData['B'] = values[1]

    #Collect RX
    device.setImpedance(inst_handler,"75K",True,"RX")
    time.sleep(WAIT_TIME_SEC_LONG)
    values = inst_handler.query_ascii_values('FETC?',
              container=np.array)
    time.sleep(WAIT_TIME_SEC)
    lcrData['R'] = values[0]
    lcrData['X'] = values[1]

    return lcrData

#DEPRECIATED 8/25/19 to keep .csv files in the same format
def standardFrequencySweep(inst_handler,arduino,frequency,voltageOscillator,voltageBias,voltageBiasOn,max_num_wells):
    WAIT_TIME_SEC=0.25
    if(voltageBiasOn == True):
        device.setBiasVoltageDC(inst_handler,voltageBias)#set bias voltage
        time.sleep(WAIT_TIME_SEC)
        #device.changeStateVoltageDC(inst_handler,1)#turn bias voltage on
        time.sleep(WAIT_TIME_SEC)
    else:
        print("Voltage Bias off")
        #device.changeStateVoltageDC(inst_handler,0)#turn bias voltage OFF
        
    device.setOscillatorVoltage(inst_handler,voltageOscillator)
    time.sleep(WAIT_TIME_SEC)

    with open('Frequency_sweep.csv', 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Frequency','Impedance','Phase','Conductance','Susceptance','Resistance','Reactance'])
        currentWell = 1
        fileWriter.writerow(['CurrentWell',currentWell])
        for currentWell in range(1,max_num_wells + 1):
            arduino.write(str(currentWell)+",")
            print(arduino.readLine())
            time.sleep(WAIT_TIME_SEC)
            for f in frequency:
                print("Frequency: " + f + "  WellNum: " + str(currentWell))
                #add in muxing later
                device.setFrequency(inst_handler,f)
                time.sleep(WAIT_TIME_SEC)
                dataDict = collectData(inst_handler)
                filewriter.writerow([f,dataDict['Z'],dataDict['P'],dataDict['G'],dataDict['B'],dataDict['R'],dataDict['X']])

def frequencyVoltageSweep(inst_handler,save_file,arduino,frequency,voltageOscillator,voltageBias,voltageBiasOn,max_num_wells):
    WAIT_TIME_SEC = .25
    if(voltageBiasOn == 1):
        device.setBiasVoltageDC(inst_handler,voltageBias)#set bias voltage
        time.sleep(WAIT_TIME_SEC)
        device.changeStateVoltageDC(inst_handler,voltageBiasOn)#turn bias voltage on
        time.sleep(WAIT_TIME_SEC)
    else:
        print("Voltage Bias off")
        device.changeStateVoltageDC(inst_handler,0)#turn bias voltage OFF
    #TO ADD:
        #Create a folder for csv files
        #Create a txt file that saves parameters
    for currentWell in range(1,max_num_wells + 1): #LOOP FOR WELLS
        command = str(currentWell)
        arduino.write(command.encode())#command.encode())
        time.sleep(1)
        print(arduino.readline())
        print(arduino.readline())
        print(arduino.readline())
        print(arduino.readline())
        time.sleep(WAIT_TIME_SEC)

        with open(save_file + '\\' + 'Well_'+str(currentWell)+'.csv', 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL,lineterminator = '\n')
            #filewriter = csv.DictWriter(csvfile,delimiter = ',',lineterminator = '\n',fieldnames=['Voltage','Frequency', 'Impedance', 'Phase', 'Conductance', 'Susceptance', 'Resistance', 'Reactance'])
            filewriter.writerow(['Voltage','Frequency', 'Impedance', 'Phase', 'Conductance', 'Susceptance', 'Resistance', 'Reactance'])

            for voltage in voltageOscillator: #LOOP FOR VOLTAGES
                device.setOscillatorVoltage(inst_handler, voltage)
                time.sleep(WAIT_TIME_SEC)

                for freq in frequency: #LOOP FOR FREQUENCIES
                    device.setFrequency(inst_handler,freq)
                    time.sleep(WAIT_TIME_SEC)
                    print("Frequency: " + freq + "  Voltage: " + voltage + "  WELL: " + str(currentWell))
                    dataDict = collectData(inst_handler)
                    filewriter.writerow([voltage,freq,dataDict['Z'],dataDict['P'],dataDict['G'],dataDict['B'],dataDict['R'],dataDict['X']])

def timeSeries(inst_handler,arduino,frequency,voltageOscillator,voltageBias,voltageBiasOn,max_num_wells,TIME_BETWEEN_SAMPLES,TIME_TOTAL):
    WAIT_TIME_SEC=0.25
    if(voltageBiasOn == 1):
        device.setBiasVoltageDC(inst_handler,voltageBias)#set bias voltage
        time.sleep(WAIT_TIME_SEC)
        device.changeStateVoltageDC(inst_handler,voltageBiasOn)#turn bias voltage on
        time.sleep(WAIT_TIME_SEC)
    else:
        print("Voltage Bias off")
        #device.changeStateVoltageDC(inst_handler,0)#turn bias voltage OFF
    evice.setOscillatorVoltage(inst_handler, voltageOscillator)
    time.sleep(WAIT_TIME_SEC)

    #CREATE FILES WITH HEADER
    for currentWell in range(1,max_num_wells+1):
        with open('Well_'+str(currentWell)+'.csv','w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            filewriter.writerow(['Time','Voltage','Frequency', 'Impedance', 'Phase', 'Conductance', 'Susceptance', 'Resistance', 'Reactance'])

    samplesTotal = int(TIME_TOTAL/TIME_BETWEEN_SAMPLES)

    for i in range(0,samplesTotal):
        for currentWell in range(1,max_num_wells + 1):
            arduino.write(str(currentWell) + ",")
            print(arduino.readLine())
            time.sleep(WAIT_TIME_SEC)
            print("samples " + str(i+1))
            dataDict = collectData(inst_handler)
            with open('Well_' + str(currentWell) + '.csv', 'a') as csvfile: # open in append mode
                filewriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                filewriter.writerow([i*TIME_BETWEEN_SAMPLES,voltageOscillator, frequency, dataDict['Z'], dataDict['P'], dataDict['G'], dataDict['B'], dataDict['R'],dataDict['X']])
            time.sleep(TIME_BETWEEN_SAMPLES)

