import device
import time
import collectionModes
import serial
import os
import csv
import datetime
'''
To change well net list, go to arduino code
"automated_mux_board_test.ino"
in function void mux_config(int well_int)
change the netList int variable to...
1: 10 well (1-10)
2: ??
3: 22 well (1-22)
4: 32 electrode chip (1-16)
'''

#Macros
EXPERIMENT_NAME = 'test3'
SAVE_PATH = os.getcwd() + '\\' + EXPERIMENT_NAME
os.mkdir(SAVE_PATH)


WELL_NETLIST_SELECTION = 3 #From Arduino Code
MAX_NUM_WELLS = 1
#FREQUENCY = ["1KHZ","25KHZ","50KHZ","100KHZ","150KHZ","250KHZ","500KHZ","750KHZ","1MHZ"]
#VOLTAGE_OSCILLATOR = ["10 MV","20 MV"]

#PREFFERED METHOD OF CREATING FREQUENCIES and VOLTAGE_OSCILLATOR
FREQUENCY_MIN = 25
FREQUENCY_STEP = 5
FREQUENCY_QUANTITY = 1
FREQUENCY = [0] * FREQUENCY_QUANTITY
for i in range(0,FREQUENCY_QUANTITY):
    FREQUENCY[i] = str(FREQUENCY_MIN + (FREQUENCY_STEP*i)) + "KHZ"
#FREQUENCY = ["10KHZ"]
print(FREQUENCY)
VOLTAGE_OSCILLATOR_MIN = 5
VOLTAGE_OSCILLATOR_STEP = 20
VOLTAGE_OSCILLATOR_QUANTITY = 1
VOLTAGE_OSCILLATOR = [0] * VOLTAGE_OSCILLATOR_QUANTITY
for i in range(0,VOLTAGE_OSCILLATOR_QUANTITY):
    VOLTAGE_OSCILLATOR[i] = str(VOLTAGE_OSCILLATOR_MIN + (VOLTAGE_OSCILLATOR_STEP*i)) + " MV"
print(VOLTAGE_OSCILLATOR)



VOLTAGE_BIAS_ON = 1
VOLTAGE_BIAS = 1.2 #in volts, dont include V
#Correction Parameters
CABLE_LENGTH = "1" #in meters
CORR_METHOD = "SING"
OPEN_CORR = False
SHORT_CORR = False
LOAD_CORR = False
LOAD_TYPE = False
CORRECTION_CHANNEL = 1
'''
    Arguments:

    inst_handle:instrument handle from 'openVisaResource()'
    cable_length: Length in meters : Positive Integer
    correction_method:SING/MULT
    open_correction:True/False
    short_correction:True/False
    load_correction:True/False
    load_type:CPD/CPQ/CPG/CPRP/CSD/CSQ/
              CSRS/LPQ/LPD/LPG/LPRP/LSD/
              LSQ/LSRS/RX/ZTD/ZTR/GB/YTD/YTR
    correction_channel: Channel to use (Integer)
'''
#Save Experiment Parameters
fil = open(SAVE_PATH + '\\' + 'readme.txt',"w+")
fil.write("EXPERIMENT NAME: " + EXPERIMENT_NAME + "\n")
fil.write("DATE: " + str(datetime.datetime.now()) + "\n")
fil.write("WELL_NETLIST_SELECTION: " + str(WELL_NETLIST_SELECTION) + "\n")
fil.write("MAX_NUM_WELLS: " + str(MAX_NUM_WELLS) + "\n")
fil.write("FREQUENCY_MIN: " + str(FREQUENCY_MIN) + "\n")
fil.write("FREQUENCY_STEP: " + str(FREQUENCY_STEP) + "\n")
fil.write("FREQUENCY_QUANTITY: " + str(FREQUENCY_QUANTITY) + "\n")
fil.write("FREQUENCY: ")
for f in FREQUENCY:
    fil.write(f + ",")
fil.write("\n")
fil.write("VOLTAGE_OSCILLATOR_MIN: " + str(VOLTAGE_OSCILLATOR_MIN) + "\n")
fil.write("VOLTAGE_OSCILLATOR_STEP: " + str(VOLTAGE_OSCILLATOR_STEP) + "\n")
fil.write("VOLTAGE_OSCILLATOR_QUANTITY: " + str(VOLTAGE_OSCILLATOR_QUANTITY) + "\n")
fil.write("VOLTAGE_OSCILLATOR: ")
for v in VOLTAGE_OSCILLATOR:
    fil.write(v + ",")
fil.write("\n")
fil.write("VOLTAGE_BIAS_ON: " + str(VOLTAGE_BIAS_ON) + "\n")
fil.write("VOLTAGE_BIAS: " + str(VOLTAGE_BIAS) + "\n")
fil.write("CABLE_LENGTH: " + CABLE_LENGTH + "\n")
fil.write("CORR_METHOD: " + str(OPEN_CORR) + "\n")
fil.write("OPEN_CORR: " + str(OPEN_CORR) + "\n")
fil.write("SHORT_CORR: " + str(SHORT_CORR) + "\n")
fil.write("LOAD_CORR: " + str(LOAD_CORR) + "\n")
fil.write("LOAD_TYPE: " + str(LOAD_TYPE) + "\n")
fil.write("CORRECTION_CHANNEL: " + str(CORRECTION_CHANNEL) + "\n")
fil.close()


#Initialization
arduino = serial.Serial('COM41', 9600, timeout=.1)
inst_handler = device.openVisaResource(17)
device_name = device.initInstrument(inst_handler,False)
inst_handler.write("*RST;*CLS")
inst_handler.write("FORM ASCII")
#device.setCorrectionParameters(inst_handler,CABLE_LENGTH,CORR_METHOD,OPEN_CORR,SHORT_CORR,LOAD_CORR,LOAD_TYPE,CORRECTION_CHANNEL):
time.sleep(3)

#get Data
#collectionModes.standardFrequencySweep(inst_handler,arduino,FREQUENCY,VOLTAGE_OSCILLATOR,VOLTAGE_BIAS,VOLTAGE_BIAS_ON,MAX_NUM_WELLS)
collectionModes.frequencyVoltageSweep(inst_handler,SAVE_PATH,arduino,FREQUENCY,VOLTAGE_OSCILLATOR,VOLTAGE_BIAS,VOLTAGE_BIAS_ON,MAX_NUM_WELLS)




'''
with open(SAVE_PATH + '\\' + 'well1.csv', 'w') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(
        ['Time', 'Voltage', 'Frequency', 'Impedance', 'Phase', 'Conductance', 'Susceptance', 'Resistance', 'Reactance'])
'''