//Daniel Mascareno
//Date: 5/16/19

//Update: wellNetlist21-> Able to access every pin on the 21 well chip
//added code for DS1804 dpot to test 

//Code that integrates assembled PCBs which need some things to change like trackstate being a 4:1 mux on the input
//different inputs and selector lines

//Update: changed WellNetlist1 to the most updated on PCB
//Update: Going to take out commas from initial input buffer and just call that string to choose what function to call
//old way: Parse through the input buffer and tokenize from the delimiter ","
//Code for entire system of automation of well switching, pump, and switching between sensing and EP

//total of 3 dpots
//x3 50k dpots X9C503
//for function generator using: AD5933 Network Analyzer I2C
//for EP using: DAC MCP4725 12 bit I2C
//for EEPROM using: 24AA01 1Kbit I2C

//Code for setting prescalar for ADC to 16 and a sample rate of 77 kHz
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#include <DigiPotX9Cxxx.h>
//#include <DS1804.h>
//#include <SPI.h>
#include "AD5933.h"

#include <String.h>

//library to do a software reset
#include <avr/wdt.h>

//this is for DAC EP
#include <Adafruit_MCP4725.h>
#include <Wire.h>
#define MCP4725_ADDR 0x60
Adafruit_MCP4725 dac;
#define DAC_RESOLUTION (12)

//AD5933 parameters
#define TWI_FREQ 400000L
#define CYCLES_BASE 15   //this was 511... now will be 15
#define CYCLES_MULTIPLIER 1  //this was 4 before
#define START_FREQUENCY 100000
#define CAL_SAMPLES 10


//Parameters for muxing through the wells
#define START_WELL 1
#define TOTAL_WELLS 28

//10k dpot variables
//--------------------------------------------
//DS1804 testPot = DS1804(2,3,4,DS1804_TEN);
//int new_resistance = 1; //to step up the resistance

//DPOT VARIABLES
//---------------------------------------------
//lines for 50k dpot of voltage divider
int CS_pot = 41;
int UP_DOWN = 42;
int INC = 43;
DigiPot v_div_Pot(INC, UP_DOWN, CS_pot);


//lines for 50k dpot of calibration
//Note: Rfb and Rcal are the same value that's why only initiate Rcal
int CS_Rcal = 35;
int UP_DOWN_Rcal = 36;
int INC_Rcal = 37;
DigiPot Rcal_Dpot(INC_Rcal, UP_DOWN_Rcal, CS_Rcal);



//ANALOG MUXING BOARD VARIABLES
//---------------------------------------------
//Shift Register variables for 16:1 muxes
//Pin connected to RCLK of 74HC595
int latchPin = 8;
//Pin connected to SRCLK of 74HC595
int clockPin = 12;
////Pin connected to SER of 74HC595
int dataPin = 11;
//Pin connected to SR_CLR_NOT of 74HC595
int SR_CLR_NOT = 7;

//pin connected to selector line of DG419 2:1 mux
//int Muxpin = 9; //have to change this to a 4:1 mux selector lines (2)
//4:1 mux that used to be a 2:1
int in_Muxpin_0 = 9;
int in_Muxpin_1 = 10;

int Muxpin2 = 6;  

int sel4mux_0 = 48;
int sel4mux_1 = 49;

int sel4mux_out_0 = 46;
int sel4mux_out_1 = 47;


//selector line for 2:1 mux to switch from Rcal and ravata chip
int calib_Mux = 34;

int analogInput = 1;   //to read in sinewave max peak
float Vout = 0;

unsigned int r_Wells[30]; //resistances for wells 1-30 e.g. r_Wells[0] => R of well 1

double calResistance = 0;

//averaging out impedances
float Z_Val_avg = 0;
float Z_Val_sum = 0;

int mux_output_mode = 25;

//int avg_calResistance = 0;
//int raw_avg_calResistance = 0;

float well_avg_Imp[11]; //to store the average impedance values and print them at the end of each round

//global for getImpedance() and AD5933_calibration_func()
double gainFactor2 = 0;
double pShift2;

//parsing parameters----------------------------------------------
const byte numChars = 32;
char receivedChars[numChars];   // an array to store the received data

boolean newData = false;

char *strings[10];
char *ptr = NULL;


int preset_EP = 0;
int well_val = 0;

int check_State_in = 0;
bool power_Status = 0;

bool EP_Status = 0;

//String well_chars;
double Z_arr[100];

double temp_Imp = 0; 


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

    // set prescale to 16
  sbi(ADCSRA,ADPS2) ;
  cbi(ADCSRA,ADPS1) ;
  cbi(ADCSRA,ADPS0) ;

  Wire.begin();

 //disable internal pull ups
 digitalWrite(SDA, LOW); 
 digitalWrite(SCL, LOW);
  
  dac.begin(MCP4725_ADDR);
  
  pinMode(analogInput, INPUT);

  //calibration mux for AD5933
  pinMode(calib_Mux, OUTPUT);
  
  //Shift REGs
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(SR_CLR_NOT, OUTPUT);

 //2:1 muxes
  //pinMode(Muxpin, OUTPUT);  //input 2:1 mux
  pinMode(in_Muxpin_0, OUTPUT);  //input 4:1 mux
  pinMode(in_Muxpin_1, OUTPUT);  //input 4:1 mux
  
  pinMode(Muxpin2, OUTPUT); // output 2:1 mux

  //4:1 muxes
  pinMode(sel4mux_0, OUTPUT);  //input 4:1 mux
  pinMode(sel4mux_1, OUTPUT);  //selector lines

  pinMode(sel4mux_out_0, OUTPUT);   //output 4:1 mux
  pinMode(sel4mux_out_1, OUTPUT);   //selector lines

  pinMode(mux_output_mode, OUTPUT); // output 2:1 mux between func gen and impedance analysis

   char receivedChars[32];

  //testPot.setToZero(); //reset DS1804
   //renewChip(); //to reset a chip or initialize as new
  // writeChip(); //writes a timestamp to the chips

//  wellNetlist21v2(26); //SO1 in and out
    
    // tell the PC we are ready
   AD5933_calibration_func(getRcalRfb());
  
  
  ////Serial.println("Arduino is ready");
  
  
  //AD5933_Func_Gen(true);
//  getImpedance();

  ////Serial.print("TIME");
  ////Serial.print(",");
  for(int j = START_WELL; j < TOTAL_WELLS+1; j++)
  { 
    if(j == 1 || j == 5 || j == 10)
    {
//      //Serial.print("REAL ");
//      //Serial.print(j);
//      //Serial.print(",");
//      //Serial.print("IMAGINARY ");
//      //Serial.print(j);
//      //Serial.print(",");
//      //Serial.print("PHASE ");
//      //Serial.print(j);
//      //Serial.print(",");
//      //Serial.print("SYSTEM PHASE ");
//      //Serial.print(j);
//      //Serial.print(",");
//      //Serial.print("CONDUCTANCE ");
//      //Serial.print(j);
//      //Serial.print(",");
//      //Serial.print("IMPEDANCE ");
//      //Serial.print(j);
//      //Serial.print(",");
    }
  }
  

  ////Serial.println();
}

int wellNumberToRead = 1;
void loop() 
{  
  //code to read new well
  /*
  if (Serial.available() > 0) {
                // read the incoming byte:
                incomingByte = Serial.read();

                // say what you got:
                //Serial.print("I received: ");
                //Serial.println(incomingByte, DEC);
        }*/
  ////Serial.print('t')
  ////Serial.print(millis() / 1000.0); 
  ////Serial.print(",");    
//  recvWithEndMarker(receivedChars); 
//  callWhichFunc(receivedChars);
 // Serial.flush();

  //temp_Imp = getImpedance(wellNumberToRead);
  ////Serial.print('i');
  delay(100);
  getImpedance(2);
  //Serial.println((int)temp_Imp);
  delay(100);
    //Be able to electroporate in between well measurements
    /*if(Serial.read() == 'p')
    {
      electroporate_Config('2',"0"); //do EP_preset 2 for all wells
    }
    */
 
 
  
}

void recvWithEndMarker(char receivedChars[]) {
    int ndx = 0;
    char endMarker = '\n';
    String recStr;
    char recChar;
       
//    while (!Serial.available()) {} //wait for input
    
    
    
    recStr = Serial.readString();
      
    for(int i = 0; i < 32; i++)
    {
        recChar = recStr[i];
      
        if (recChar == endMarker) {
            receivedChars[ndx] = '\0';
            return; 
        } else if(recChar == ',') {
            continue;
        }

        receivedChars[ndx] = recChar;
        ndx++;
    }
}

//void callWhichFunc(char receivedChars[])
//{
//  char func = receivedChars[0];
//  String well_chars;  //clear the string//this is for well numbers
//   
//  switch (func)
//  {    
//      case 'A':
//          checkEEPROM();
//          break;
//
//      case 'B': 
//           checkState(receivedChars[1]);
//          break;
//
//      case 'C':
//           well_chars.concat(receivedChars[1]);
//           well_chars.concat(receivedChars[2]);
//           getVoltage(well_chars);
//           break;
//
//       case 'D':
//          getRcalRfb();
//          break;
//
//       case 'E':
//          AD5933_calibration_func(getRcalRfb());
//          break;
//
//       case 'F':
//          well_chars.concat(receivedChars[1]);
//          well_chars.concat(receivedChars[2]);
//
//          for(int x = 0; x < 100; x++)
//          {
//           getImpedance(well_chars);
//          }
//          //Serial.println();
//          
////          for(int y = 0; y < 100; y++)
////          {
////            //Serial.print(Z_arr[y]);
////            //Serial.print(",");
////          }
////
////           //Serial.println();
////          
//          break;
//      
//       case 'G':
//          well_chars.concat(receivedChars[2]);
//          well_chars.concat(receivedChars[3]);
//          electroporate_Config(receivedChars[1], well_chars);  //have to send a two char string!
//          break;
//
//       
//       case 'H':
//          writeChip();
//          break;
//       
//       case 'R':
//          softwareReset(WDTO_60MS);
//          break;
//
//       default: break;
//  }
//}



//getVoltage for a certain wel_numb
//reading max voltage of the sinewave after it passes through Ravata chips
//using Arduino ADC
void getVoltage(String well_char)
{
  double rawV = 0;
  double vChip = 0;
  double vChipMax = 0;

  int well_numb = well_char.toInt();
  double getVolt_arr[100];

  
  wellNetlist21(well_numb); //change muxes to well_numb
  
  //set input 4:1 mux S3
  digitalWrite(sel4mux_0, LOW);
  digitalWrite(sel4mux_1, HIGH);
  
  //switch 4:1 output mux to Arduino ADC
  digitalWrite(sel4mux_out_0,HIGH);
  digitalWrite(sel4mux_out_1,HIGH);
  
  //switch to ravata chip (calib mux)
  digitalWrite(calib_Mux, LOW);
  
  //start the func gen
  AD5933_Func_Gen(true);
  ////Serial.println("Turned on Func gen");
  //for loop to get the Vmax of sinewave
   
  for(int j = 0; j < 100; j++)
  { 
     for(int i = 0; i < 1000; i++)
     { 
        rawV = analogRead(A4);
        vChip = (5.0 * rawV) / 1024.0;
  
       // //Serial.println(vChipMax);
        // Check for new max
        if(vChip > vChipMax)
        {
          vChipMax = vChip;
        }
        
     }
      getVolt_arr[j] = vChipMax;
     
  }

  for(int k = 0; k < 100; k++) {
    //Serial.print(getVolt_arr[k]);
    //Serial.print(",");
  }
  //Serial.println();


  //turn off Func gen
  AD5933_Func_Gen(false);
  
  ////Serial.println(vChipMax);
  
}


// this is for the ADC voltage divider circuit input
//-------------------------------------------------------------------------------------------------------------------------------------------------
void AD5933_Func_Gen(bool mode)
{
  const int numofIncrement = 10;
  const double calResistance = 6780;
  double gainFactor, pShift;

  if (mode == true)
  {
    //setByte(0x81, 0x18); // Reset & Use Ext. Clock - 0001 1000
    AD5933.setExtClock(false);
    AD5933.resetAD5933();
    AD5933.setStartFreq(START_FREQUENCY);
    AD5933.setSettlingCycles(CYCLES_BASE, CYCLES_MULTIPLIER);
    AD5933.setStepSizeInHex(1);
    AD5933.setNumofIncrement(2);
    AD5933.setPGA(GAIN_1);
    AD5933.setRange(RANGE_1);
    
    //gainFactor = AD5933.getGainFactor(calResistance,1);
    AD5933.getGainFactor(calResistance, CAL_SAMPLES, gainFactor, pShift, false);
    
//    AD5933.tempUpdate();
//    AD5933.setCtrMode(REPEAT_FREQ);
  }

  else
  {
    AD5933.setCtrMode(STAND_BY);
  }
  
}

double getRcalRfb()
{ 
  double calResistance = 0;
  int avg_calResistance = 0;
  int raw_avg_calResistance = 0;

 //set input 4:1 mux selector lines to S3
  digitalWrite(sel4mux_0, LOW);
  digitalWrite(sel4mux_1, HIGH);

  //set output 4:1 mux selector lines to NO2 (AD5933 module)
  digitalWrite(sel4mux_out_0, LOW);
  digitalWrite(sel4mux_out_1, HIGH);


  //switch to func gen (voltage divider)
  digitalWrite(mux_output_mode, LOW);

  //switch to ravata chip
  digitalWrite(calib_Mux, LOW);

  //start the func gen for input to voltage divider
  AD5933_Func_Gen(true);

  //set dpots to step 1
  
  v_div_Pot.reset(); 
  Rcal_Dpot.reset();
 
  //get the step values for rcal and rfb for all wells
//  for(int k = 1; k < 22; k++)
  //{
    int k = 2;
//    wellNetlist21(k);  //go to the k+1 well 
//    //Serial.print("Well # is: ");
//    //Serial.println(k);
    r_Wells[k] = get_well_Resistance(); //step number for well 2 dpots (Rcal and Rfb)
//    //Serial.println("Stopping here to debug If completed, press p and Enter>");
//    while( Serial.read() != 'p')
//     ;
//    raw_avg_calResistance = raw_avg_calResistance + r_Wells[k];  
    
//    //Serial.print("WE got the well resistance: ");
//    //Serial.println(r_Wells[k]);
    v_div_Pot.reset(); 
     
  //} //for loop close bracket

  avg_calResistance = raw_avg_calResistance / 17; // this gives you the average number of step size //divide by the number of wells

  ////Serial.print("The average step size for all wells is: ");
   
//  
  avg_calResistance = avg_calResistance + 2; // add 1k for cushion
 //avg_calResistance = 4;
  //avg_calResistance = 7;
  avg_calResistance = 10;
  //avg_calResistance = 12;
  //avg_calResistance = 50;
  //Serial.println(avg_calResistance); 
  calResistance = avg_calResistance*500;
  
  //set Rfb dpot and Rcal dpot
  Rcal_Dpot.increase(avg_calResistance);

   AD5933_Func_Gen(false);
   //turn off AD5933

  
  ////Serial.println(calResistance);
  return calResistance;
   //switch to AD5933 impedance analysis
   //digitalWrite(mux_output_mode, HIGH);
}


//function to calculate the step number we need to set for Rcal and Rfb dpots based on the well resistance
//-------------------------------------------------------------------------------------------------------------------------------------------------
unsigned int get_well_Resistance()
{
  int Vout_raw = 0;
  float max_volt = 0;
  float current_val = 0;
  float prev_val = 0;
  unsigned int well_step_Num = 0;
  long current_val_new = 0;
  long prev_val_new = 0;
      
  while( 1 ) //this range varies depending on chip empty vs chip full (NOTE: 1.5 and 1.7 is the range for 1/2 Vdd for Range 2)
  {
      ////Serial.println("In the while");
      
//      //Serial.println("Stopping here to debug If completed, press p and Enter>");
//      while( Serial.read() != 'p')
//       ;
     
     for(int k = 0; k < 1000; k++)
     { 
      Vout_raw = analogRead(analogInput); //analog input A1
      Vout = (Vout_raw * 5.0 ) / 1024.0;
         
       // Check for new max
      if(Vout > max_volt)
      {
        max_volt = Vout;
      }
   }
   ////Serial.println(max_volt,4);
  if(max_volt > 0)
  {
    break;
  }

//    current_val = max_volt;
//    //cannot compare with "==" using floats so converting them to longs
//    current_val_new = current_val*10000;
//    prev_val_new = prev_val*10000;
////    //Serial.print("comparing: ");
////    //Serial.println(current_val_new);
////    //Serial.println(prev_val_new);  
//   if(v_div_Pot.get() > 4 && current_val_new == prev_val_new) //after 4 steps and we get a repeat
//   {
//
//  //  //Serial.println(max_volt);
//    // v_div_Pot.reset(); 
//     break;  
//   }

     v_div_Pot.increase(1); 
//     //Serial.println(max_volt, 4);
     ////Serial.println(v_div_Pot.get());
     //prev_val = current_val;
  }  
    
    ////Serial.println(max_volt, 4);
    well_step_Num = v_div_Pot.get(); //returns the step number of dpot for Rcal and Rfb
    ////Serial.println(well_step_Num);
  return well_step_Num;
  
}

//before calling AD5933_calibration__func make sure you call getRcalRfb function to set dpots
void AD5933_calibration_func(double dpotcalibR)
{
  //switch to AD5933 impedance analysis
  digitalWrite(mux_output_mode, HIGH);
  //AD5933 parameters
const int numofIncrement = 10;



//setup code for AD5933
  AD5933.setExtClock(false);
  AD5933.resetAD5933();
  AD5933.setStartFreq(START_FREQUENCY);
  AD5933.setSettlingCycles(CYCLES_BASE, CYCLES_MULTIPLIER);
  AD5933.setStepSizeInHex(1);
  AD5933.setNumofIncrement(2);
  AD5933.setPGA(GAIN_1);
  AD5933.setRange(RANGE_1);

  //switch the 2:1 mux to the rcal
  digitalWrite(calib_Mux, HIGH);

  ////Serial.println("b4 getting gain factor");
  ////Serial.println(dpotcalibR);
  //gainFactor = AD5933.getGainFactor(calResistance,1);
  AD5933.getGainFactor(dpotcalibR, CAL_SAMPLES, gainFactor2, pShift2, false); //line to get gain factor

  ////Serial.println("after getting gain factor");
  //gainFactor = 6915880;
  //gainFactor = 6976540;
 // newgainFactor = double(float(1)/float(gainFactor));
 // gainFactor2 = 19420996.00; //step size of 4 @ 50 kHz at range 1
//  //Serial.print("Gain Factor: ");
 // gainFactor2 = 19171118.00; //step size of 4 @ 100 khZ at range 1
  gainFactor2 = 48535944.00; //step size of 10 @ 100 kHz at range 1
 // gainFactor2 = 8833925.00; //step size of 4 @ 100 khZ at range 2
 //gainFactor2 = 58385632.00; //step size of 12 @ 100 khz at range 1

  ////Serial.println(gainFactor2);

  ////Serial.print("System Phase Shift: ");
  ////Serial.println(pShift2);
//  //Serial.println(float2s(gainFactor,5));
//  //Serial.print("System Phase Shift: ");
//  //Serial.println(pShift);


  //switch the 2:1 mux to the ravata chip
  digitalWrite(calib_Mux ,LOW);  

 //AD5933.tempUpdate();
 //AD5933.setCtrMode(REPEAT_FREQ);
  
}

//getImpedance for a certain well_num
//before calling this function you must call ad5933 calibration function
//-------------------------------------------------------------------------------------------------------------------------------------------------
double getImpedance(int well_char)
{
  double electricalResistivity = 0;
  double wellConductivity = 0;
  double realPart = 0;
  double imagPart = 0;
  
  int *p; // pointer to int to read Real and imaginary register from AD5933 library
  //gndState();
  //set input 4:1 mux selector lines S3
   digitalWrite(sel4mux_0, LOW);
   digitalWrite(sel4mux_1, HIGH);
  
   //set output 4:1 mux selector lines S3
   digitalWrite(sel4mux_out_0, LOW);
   digitalWrite(sel4mux_out_1, HIGH);
  
   AD5933.setCtrMode(REPEAT_FREQ);

    ////Serial.println(well_char);
   

//   //Serial.print("Well num for get imp: ");
//   //Serial.println(well_num);
  wellNetlist21(well_char);  //go to a certain well   
  
  
  double Z_Imp, phase, pShift, currPhase;

  //once you get impedance for a certain well: GROUND the electrodes
  //switch muxes
//  for(int i = 0; i < 100; i++)
//  {

//  //Serial.print(millis() / 1000.0); 
//  //Serial.print("\t"); 
//  
//  for(int i = 5; i < 21; i++)
//  {
  
    p = AD5933.getComplex(gainFactor2, pShift2, Z_Imp, phase);



    imagPart = Z_Imp * sin(phase);
    realPart = Z_Imp * cos(phase);
    electricalResistivity = realPart * (6255 / 94.983);
    
    wellConductivity = 1 / (electricalResistivity);
    
    
    // Print raw frequency data
//          //Serial.print(cfreq);
//          //Serial.print(":");

 
    ////Serial.print(realPart);
    
    ////Serial.print(",");
    ////Serial.print(imagPart);

    ////Serial.print(",");
    //Serial.println((int) phase*19.098593);
    Serial.println(realPart);

 
    ////Serial.print(","); 
    currPhase = atan2(*(p+1), *p);
    ////Serial.print(currPhase);
    ////Serial.print(","); 
    
    ////Serial.print(wellConductivity*10000.0,3);

    ////Serial.print(",");

   // We need Z_Imp for each well
   ////Serial.print(phase);
   
  //}

 // }

 // //Serial.println();
   return Z_Imp;

//  for(int m = 0; m < 100; m++) {
//    //Serial.print(imp_arr[m]);
//    //Serial.print(",");
//  }
//  //Serial.println();
  //gndState();
   //AD5933.setCtrMode(STAND_BY);
    //testPot.setWiperPosition( new_resistance );
  //  delay(1); // give DS1804 time to change, and for analogue reading to settle

   // new_resistance++;


}

//gnd in between getImpedance and EP pulses
void gndState()
{
   //input 4:1 mux sel lines
   digitalWrite(sel4mux_0,HIGH);
   digitalWrite(sel4mux_1,LOW);
  
   //output 4:1 mux sel lines
   digitalWrite(sel4mux_out_0,HIGH);
   digitalWrite(sel4mux_out_1,LOW);

   delay(1);
}


//most updated: 5/1/19 function to configure muxes
//-------------------------------------------------------------------------------------------------------------------------------------------------
void wellNetlist1(int well_int)
{
  byte data1 = 0;
  byte data2 = 0;
  
  //netlist 1 is for 10 well chips
    if(well_int <= 8)
    {
      digitalWrite(in_Muxpin_0, LOW);
      digitalWrite(in_Muxpin_1, LOW);
      
      digitalWrite(Muxpin2, LOW);
       
      switch(well_int)
      {
        case 1:
          data1 = 0x0E;  //output mux
          data2 = 0x07;  //input mux
          break;
            
        case 2:
          data1 = 0x0C;  //output mux
          data2 = 0x05;  //input mux
          break;
      
         case 3:
          data1 = 0x0A;
          data2 = 0x03;
          break;
      
        case 4:
          data1 = 0x08;
          data2 = 0x01;
          break;
      
         case 5:
          data1 = 0x01; //output mux
          data2 = 0x08; //input mux
          break;
      
        case 6:
          data1 = 0x03; //output mux
          data2 = 0x0A; //input mux
          break;
      
         case 7:
         data1 = 0x05; //output mux
         data2 = 0x0C; //input mux
         break;
      
        case 8:
         data1 = 0x07; //output mux
         data2 = 0x0E;  //input mux
         break;
                
        default:
          break;
        }
     
     }
    
     else
     { 
        digitalWrite(in_Muxpin_0, HIGH);
        digitalWrite(in_Muxpin_1, HIGH);
        
        digitalWrite(Muxpin2, HIGH);
        
        switch(well_int)
        {
         case 9:
            data1 = 0xE0; //output mux
            data2 = 0x70;  //input mux
            break;
  
          case 10:
            data1 = 0xC0; //output mux
            data2 = 0x50;  //input mux
            
            break;  

          default:
            break;      

        }
      }
  
  digitalWrite(SR_CLR_NOT, 1);
  digitalWrite(latchPin, 0);
  //stageBipolar(addr_Electroporation); /// selector lines for 4:1 mux (2 lower bits) 00: 5V, 01: GND, 10: -5V, 10: NC
  shiftOut(dataPin, clockPin, MSBFIRST, data1); //selector lines for MUX 1 (4 lower bits) and 2 (4 upper bits)
  shiftOut(dataPin, clockPin, MSBFIRST, data2); //selector lines for MUX 3 (lower 4 bits)
  digitalWrite(latchPin, 1);
  digitalWrite(SR_CLR_NOT, 0);

}

//update:added this on 5/6/19 of 21 well netlist
void wellNetlist21(int well_int)
{
      byte data1 = 0;
      byte data2 = 0;
      //for the first 13 wells we need to set the 2:1 muxes low
      if(well_int <= 13)
      {
       digitalWrite(in_Muxpin_0, LOW);
       digitalWrite(in_Muxpin_1, LOW);
      
       digitalWrite(Muxpin2, LOW);
       
       //cases are well numbers
       switch(well_int)
        {
          case 1:
            data1 = 0x0F; //output mux
            data2 = 0x07;  //input mux
            break;
        
          case 2:
            data1 = 0x0E;  //output mux
            data2 = 0x06;  //input mux
            break;
        
           case 3:
            data1 = 0x0D;  //output mux
            data2 = 0x05;  //input mux
            break;
        
          case 4:
            data1 = 0x0C; //output mux
            data2 = 0x04;  //input mux
            break;
        
           case 5:
            data1 = 0x0B;  //output mux
            data2 = 0x03;  //input mux
            break;
        
          case 6:
            data1 = 0x0A;  //output mux
            data2 = 0x02;  //input mux
            break;
        
           case 7:
            data1 = 0x09;  //output mux
            data2 = 0x01;  //input mux
            break;
        
          case 8:
            data1 = 0x08;  //ouput mux
            data2 = 0x00;  //input mux
            break;
        
          case 9:
            data1 = 0x00;  //output mux
            data2 = 0x08;  //input mux
            break;
        
          case 10:
            data1 = 0x01;  //output mux
            data2 = 0x09;  //input mux
            break;
    
          case 11:
            data1 = 0x02;  //output mux
            data2 = 0x0A;  //input mux
            break;
    
          case 12:
            data1 = 0x06; //output mux
            data2 = 0x0E; //input mux
            break;
    
          case 13:
            data1 = 0x07;  //output mux
            data2 = 0x0F;  //input mux
            break;
            
          default:
            ////Serial.println("This is not a valid well");
            break;
        }
      }
    
      else
      {
       digitalWrite(in_Muxpin_0, HIGH);
       digitalWrite(in_Muxpin_1, HIGH);
       
       digitalWrite(Muxpin2, HIGH);
       
       switch(well_int)
        {
          case 14:
            data1 = 0xF0;  //output mux
            data2 = 0x70;  //input mux
            break;
        
          case 15:
            data1 = 0xE0;  //output mux
            data2 = 0x60;   //input mux
            break;
        
           case 16:
            data1 = 0xD0;  //output mux
            data2 = 0x50;  //input mux
            break;
        
          case 17:
            data1 = 0xC0;   //output mux
            data2 = 0x40;   //input mux
            break;
        
           case 18:
            data1 = 0xB0;   //output mux
            data2 = 0x30;   //input mux
            break;
        
          case 19:
            data1 = 0xA0;   //output mux
            data2 = 0x20;   //input mux
            break;
        
          case 20:
            data1 = 0x90;   //output mux
            data2 = 0x10;   //input mux
            break;

          case 21:
            data1 = 0x80;  //output mux
            data2 = 0x00;  //input mux
            break;

          case 22:
            data1 = 0x00;  //output mux
            data2 = 0x80;  //input mux
            break;

          case 23: //actually SI3 In and out (pin 12)
            digitalWrite(in_Muxpin_0, LOW);
            digitalWrite(in_Muxpin_1, LOW);
            digitalWrite(Muxpin2, LOW);
             
            data1 = 0x03; //output mux
            data2 = 0x0B; //input mux
            break;

          case 24: //actually SI2 In and out (pin13)
            digitalWrite(in_Muxpin_0, LOW);
            digitalWrite(in_Muxpin_1, LOW);
            digitalWrite(Muxpin2, LOW);
             
            data1 = 0x04; //output mux
            data2 = 0x0C; //input mux
            break;

          case 25: //actually SI1 In and out (pin14)
            digitalWrite(in_Muxpin_0, LOW);
            digitalWrite(in_Muxpin_1, LOW);
            digitalWrite(Muxpin2, LOW);
             
            data1 = 0x05; //output mux
            data2 = 0x0D; //input mux
            break;

          case 26: //actually SO2 In and out (pin26)
            data1 = 0x10; //output mux
            data2 = 0x90; //input mux
            break;

          case 27: //actually SO1 In and out (pin27)  
            data1 = 0x20; //output mux
            data2 = 0xA0; //input mux
            break;

          case 28: //actually Short In and Short out (pin28)
            data1 = 0x30; //output mux
            data2 = 0xB0; //input mux
            break;
              
          default:
            ////Serial.println("This is not a valid well");
            break;
        }
      }
      
  digitalWrite(SR_CLR_NOT, 1);
  digitalWrite(latchPin, 0);
  //stageBipolar(addr_Electroporation); /// selector lines for 4:1 mux (2 lower bits) 00: 5V, 01: GND, 10: -5V, 10: NC
  shiftOut(dataPin, clockPin, MSBFIRST, data1); //selector lines for MUX 1 (4 lower bits) and 2 (4 upper bits)
  shiftOut(dataPin, clockPin, MSBFIRST, data2); //selector lines for MUX 3 (lower 4 bits)
  digitalWrite(latchPin, 1);
  digitalWrite(SR_CLR_NOT, 0);
  
}

//update:added this on 5/8/19 of 21 well netlist b/c of assembly mistake
//only can use wells 5-22 b/c of this
void wellNetlist21v2(int well_int)
{
      byte data1 = 0;
      byte data2 = 0;
      //for the first 13 wells we need to set the output 2:1 mux and input 4:1 mux HIGH
      if(well_int <= 13)  //have to change this!
      {
       digitalWrite(in_Muxpin_0, HIGH);
       digitalWrite(in_Muxpin_1, HIGH);
      
       digitalWrite(Muxpin2, HIGH);
       
       //cases are well numbers
       switch(well_int)
       {
        //note case 1-3 are not wells they are actually sensor in
           case 1: //SI1 in and out
            data1 = 0xD0; //output mux
            data2 = 0x50; //input mux
            break;

          case 2: //SI2 in and out
            data1 = 0xC0; //output mux
            data2 = 0x40;  //input mux
            break;

          case 3: //SI3 in and out
            data1 = 0xB0; //output mux
            data2 = 0x30; //input mux
            break;

          case 4:
            break; // nothing here
           
           case 5:
            data1 = 0x30;  //output mux
            data2 = 0xB0;  //input mux
            break;
        
          case 6:
            data1 = 0x20;  //output mux
            data2 = 0xA0;  //input mux
            break;
        
           case 7:
            data1 = 0x10;  //output mux
            data2 = 0x90;  //input mux
            break;
        
          case 8:
            data1 = 0x00;  //ouput mux
            data2 = 0x80;  //input mux
            break;
        
          case 9:
            data1 = 0x80;  //output mux
            data2 = 0x00;  //input mux
            break;
        
          case 10:
            data1 = 0x90;  //output mux
            data2 = 0x10;  //input mux
            break;
    
          case 11:
            data1 = 0xA0;  //output mux
            data2 = 0x20;  //input mux
            break;
    
          case 12:
            data1 = 0xE0; //output mux
            data2 = 0x60; //input mux
            break;
    
          case 13:
            data1 = 0xF0;  //output mux
            data2 = 0x70;  //input mux
            break;
            
          default:
            ////Serial.println("This is not a valid well");
            break;
        }
      }
    
      else
      {
       digitalWrite(in_Muxpin_0, LOW);
       digitalWrite(in_Muxpin_1, LOW);
       
       digitalWrite(Muxpin2, LOW);
       
       switch(well_int)
        {
          case 14:
            data1 = 0x07;  //output mux
            data2 = 0x0F;  //input mux
            break;
        
          case 15:
            data1 = 0x06;  //output mux
            data2 = 0x0E;   //input mux
            break;
        
           case 16:
            data1 = 0x05;  //output mux
            data2 = 0x0D;  //input mux
            break;
        
          case 17:
            data1 = 0x04;   //output mux
            data2 = 0x0C;   //input mux
            break;
        
           case 18:
            data1 = 0x03;   //output mux
            data2 = 0x0B;   //input mux
            break;
        
          case 19:
            data1 = 0x02;   //output mux
            data2 = 0x0A;   //input mux
            break;
        
           case 20:
            data1 = 0x01;   //output mux
            data2 = 0x09;   //input mux
            break;

           case 21:
            data1 = 0x00;  //output mux
            data2 = 0x08;  //input mux
            break;

           case 22:
            data1 = 0x08;  //output mux
            data2 = 0x00;  //input mux
            break;

           case 23: //short in and short out well
            data1 = 0x0B; //output mux
            data2 = 0x03; //input mux
            break;

           case 24:  //SO1 out and in
            data1 = 0x0A; //output mux
            data2 = 0x02; //input mux
            break;

           case 25: //SO2 out and in
            data1 = 0x09;  //output mux
            data2 = 0x01;  //input mux
            break;
      
          default:
            //Serial.println("This is not a valid well");
            break;
        }
      }
      
  digitalWrite(SR_CLR_NOT, 1);
  digitalWrite(latchPin, 0);
  //stageBipolar(addr_Electroporation); /// selector lines for 4:1 mux (2 lower bits) 00: 5V, 01: GND, 10: -5V, 10: NC
  shiftOut(dataPin, clockPin, MSBFIRST, data1); //selector lines for MUX 1 (4 lower bits) and 2 (4 upper bits)
  shiftOut(dataPin, clockPin, MSBFIRST, data2); //selector lines for MUX 3 (lower 4 bits)
  digitalWrite(latchPin, 1);
  digitalWrite(SR_CLR_NOT, 0);
  
}


//will have to change later to account for different wiring of muxes on PCB
void wellNetlist2(int well_int)
{
      byte data1 = 0;
      byte data2 = 0;
      //for the first 13 wells we need to set the 2:1 muxes low
      if(well_int <= 13)
      {
       digitalWrite(in_Muxpin_0, LOW);
       digitalWrite(in_Muxpin_1, LOW);
      
       digitalWrite(Muxpin2, LOW);
       
       //cases are well numbers
       switch(well_int)
        {
          case 1:
            data1 = 0x00;
            data2 = 0x08;
            break;
        
          case 2:
            data1 = 0x01;
            data2 = 0x09;
            break;
        
           case 3:
            data1 = 0x02;
            data2 = 0x0A;
            break;
        
          case 4:
            data1 = 0x03;
            data2 = 0x0B;
            break;
        
           case 5:
            data1 = 0x04;
            data2 = 0x0C;
            break;
        
          case 6:
            data1 = 0x05;
            data2 = 0x0D;
            break;
        
           case 7:
            data1 = 0x06;
            data2 = 0x0E;
            break;
        
          case 8:
            data1 = 0x07;
            data2 = 0x0F;
            break;
        
          case 9:
            data1 = 0x0F;
            data2 = 0x07;
            break;
        
          case 10:
            data1 = 0x0E;
            data2 = 0x06;
            break;
    
          case 11:
            data1 = 0x0A;
            data2 = 0x02;
            break;
    
          case 12:
            data1 = 0x09;
            data2 = 0x01;
            break;
    
          case 13:
            data1 = 0x08;
            data2 = 0x00;
            break;
            
          default:
            //Serial.println("This is not a valid well");
            break;
        }
      }
    
      else
      {
       digitalWrite(in_Muxpin_0, HIGH);
       digitalWrite(in_Muxpin_1, HIGH);
       
       digitalWrite(Muxpin2, HIGH);
       
       switch(well_int)
        {
          case 14:
            data1 = 0x70;
            data2 = 0x80;
            break;
        
          case 15:
            data1 = 0x60;
            data2 = 0x90;
            break;
        
           case 16:
            data1 = 0x50;
            data2 = 0xA0;
            break;
        
          case 17:
            data1 = 0x40;
            data2 = 0xB0;
            break;
        
           case 18:
            data1 = 0x30;
            data2 = 0xC0;
            break;
        
          case 19:
            data1 = 0x20;
            data2 = 0xD0;
            break;
        
           case 20:
            data1 = 0x10;
            data2 = 0xE0;
            break;
        
          default:
            //Serial.println("This is not a valid well");
            break;
        }
      }
      
  digitalWrite(SR_CLR_NOT, 1);
  digitalWrite(latchPin, 0);
  //stageBipolar(addr_Electroporation); /// selector lines for 4:1 mux (2 lower bits) 00: 5V, 01: GND, 10: -5V, 10: NC
  shiftOut(dataPin, clockPin, MSBFIRST, data1); //selector lines for MUX 1 (4 lower bits) and 2 (4 upper bits)
  shiftOut(dataPin, clockPin, MSBFIRST, data2); //selector lines for MUX 3 (lower 4 bits)
  digitalWrite(latchPin, 1);
  digitalWrite(SR_CLR_NOT, 0);
  
}




//function to make pulse of amplitude V (ranging from +5v to -5V) 
//width is # of milliseconds of each pulse (ranging from 1 - 1000 ms)
//---------------------------------------------------------------b----------------------------------------------------------------------------------

void EPpulse(float amplitude, int width)
{
  //gndState();
   
   //input 4:1 mux sel lines
  digitalWrite(sel4mux_0,LOW);
  digitalWrite(sel4mux_1,LOW);
  
  //output 4:1 mux sel lines
  digitalWrite(sel4mux_out_0, LOW);
  digitalWrite(sel4mux_out_1, LOW);
  
  int baseline_amp = 2047; //zero baseline
  int new_amp = 0;

  int raw_amp = amplitude*409;
  
  new_amp = baseline_amp + raw_amp; 
  dac.setVoltage(new_amp,false);
  delay(width);
  //gndState();

}

//to call different presets for electroporation
void electroporate_Config(char preset_num, String well_num)
{
  //Serial.println("EP starting");
  int converChar = well_num.toInt();  //convert char to int

  //input 4:1 mux sel lines
  digitalWrite(sel4mux_0,LOW);
  digitalWrite(sel4mux_1,LOW);
  
  //output 4:1 mux sel lines
  digitalWrite(sel4mux_out_0,LOW);
  digitalWrite(sel4mux_out_1, LOW);

  //wellNetlist21(converChar);
  
  switch(preset_num)
  {
    case '1':
        //gndState();
        EP_Preset1(converChar);
        //gndState();
        break;

    case '2':
        //gndState();
        EP_Preset2(converChar);
        //gndState();
        break;

    case '3':
        //gndState();
        EP_Preset3(converChar);
        //gndState();
        break;

    case '4':
        //gndState();
        EP_Preset4(converChar);
        //gndState();
        break;

    case '5':
        //gndState();
        EP_Preset5(converChar);
        //gndState();
        break;

    default: break;
  }
}

//EP Preset 1
void EP_Preset1(int well_num)
{
  wellNetlist21(well_num);

  EPpulse(4, 10);
  EPpulse(0, 20);
  EPpulse(-3, 22);
  EPpulse(0, 5);
  EPpulse(0.5, 30);
  EPpulse(-1, 50);
  EPpulse(0, 5);
  EPpulse(5, 1);
  EPpulse(0, 2);
  EPpulse(-4.8, 25);
  EPpulse(0,100);
}

//EP Preset 2
void EP_Preset2(int well_num)
{
 //Serial.print(millis() / 1000.0); 
 //Serial.print(",");     
 for(int m = START_WELL; m < TOTAL_WELLS+1; m++) 
 {
    //Serial.print(0); //real
    //Serial.print(",");
    //Serial.print(0);  //imag
    //Serial.print(",");
    //Serial.print(0); //phase1
    //Serial.print(",");
    //Serial.print(0);  //system phase
    //Serial.print(",");
    //Serial.print(0);  //impedance
    //Serial.print(",");
    wellNetlist21(m);
  
   for(int k = 0; k < 3; k++)
   { 
    for(int j = 0; j < 5; j++)
    {
      EPpulse(3, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      
    }
   }
   delay(10);
 }
 
 //Serial.println();
}

//EP Preset 3
void EP_Preset3(int well_num)
{

 //Serial.print(millis() / 1000.0); 
 //Serial.print(",");     
 for(int m = START_WELL; m < TOTAL_WELLS+1; m++) 
 {
    //Serial.print(0); //real
    //Serial.print(",");
    //Serial.print(0);  //imag
    //Serial.print(",");
    //Serial.print(0); //phase1
    //Serial.print(",");
    //Serial.print(0);  //system phase
    //Serial.print(",");
    //Serial.print(0);  //impedance
    //Serial.print(",");
    wellNetlist21(m);
  
   //for(int k = 0; k < 2; k++)
   //{ 
    for(int j = 0; j < 3; j++)
    {
      EPpulse(2.9, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-2.9, 6); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      
    }
    EPpulse(1,5);
    EPpulse(0,100);
    EPpulse(-1,5);
    EPpulse(0,100);
    EPpulse(0,5);
    EPpulse(0,100);
  // }
   delay(10);
 }
 
 //Serial.println();
}

void EP_Preset4(int well_num)
{
  //Serial.print(millis() / 1000.0); 
 //Serial.print(",");
 //Serial.print("In 4");     
 for(int m = START_WELL; m < TOTAL_WELLS+1; m++) 
 {
    //Serial.print("Next well");    
    //Serial.print(0); //real
    //Serial.print(",");
    //Serial.print(0);  //imag
    //Serial.print(",");
    //Serial.print(0); //phase1
    //Serial.print(",");
    //Serial.print(0);  //system phase
    //Serial.print(",");
    //Serial.print(0);  //impedance
    //Serial.print(",");
    wellNetlist21(m);
  
   for(int k = 0; k < 2; k++)
   { 
    for(int j = 0; j < 3; j++)
    {
      //Serial.print("In electroporation loop");
      EPpulse(2.8, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-2.8, 6); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms

      
      EPpulse(3.2, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-3.2, 6); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms

      EPpulse(0.75, 6);
      EPpulse(0, 200);
      EPpulse(-0.75, 6);
      EPpulse(0, 200);

      EPpulse(0.75, 6);
      EPpulse(0, 200);
      EPpulse(-0.75, 6);
      EPpulse(0, 200);
        
    }
    EPpulse(1,5);
    EPpulse(0,100);
    EPpulse(-1,5);
    EPpulse(0,100);
    EPpulse(0,5);
    EPpulse(0,100);
  
    delay(2000); //2 second delay between each 
  }
//   delay(2000);
 }
 
 //Serial.println();


  
//  wellNetlist21(well_num);
//
//  for(int i = 0; i < 3; i++)
//  {
//    EPpulse(5,2);
//    EPpulse(0,10);
//  }
//
//  for(int j = 0; j < 3; j++)
//  {
//    EPpulse(-5,2);
//    EPpulse(0,10);
//  }
//
//  EPpulse(3,6);
//  EPpulse(0,50);
//
//  for(int k = 0; k < 4; k++)
//  {
//    EPpulse(2,50);
//    EPpulse(0,50);
//  }
//
//  EPpulse(-3,6);
//  EPpulse(0,50);
//
//  for(int m = 0; m < 4; m++)
//  {
//    EPpulse(-2,50);
//    EPpulse(0,50);
//  }
}

void EP_Preset5(int well_num)
{
  //Serial.print(millis() / 1000.0); 
 //Serial.print(",");     
 for(int m = START_WELL; m < TOTAL_WELLS+1; m++) 
 {
    //Serial.print(0); //real
    //Serial.print(",");
    //Serial.print(0);  //imag
    //Serial.print(",");
    //Serial.print(0); //phase1
    //Serial.print(",");
    //Serial.print(0);  //system phase
    //Serial.print(",");
    //Serial.print(0);  //impedance
    //Serial.print(",");
    wellNetlist21(m);
  
   for(int k = 0; k < 2; k++)
   { 
    for(int j = 0; j < 3; j++)
    {
      EPpulse(2.8, 8);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-2.8, 8); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms

      
      EPpulse(3, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-3, 6); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms

      EPpulse(3.2, 6);  //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms
      EPpulse(-3.2, 6); //3V and 6 ms
      EPpulse(0, 100); //0V and 100 ms

      EPpulse(0.75, 6);
      EPpulse(0, 50);
      EPpulse(-0.75, 6);
      EPpulse(0, 50);

      EPpulse(0.75, 6);
      EPpulse(0, 50);
      EPpulse(-0.75, 6);
      EPpulse(0, 50);
        
    }
    EPpulse(1,5);
    EPpulse(0,100);
    EPpulse(-1,5);
    EPpulse(0,100);
    EPpulse(0,5);
    EPpulse(0,100);
  
    delay(1000); //1 second delay between each 
  }
//   delay(2000);
 }
 
 //Serial.println();


  
//  wellNetlist21(well_num);
//
//  for(int i = 0; i < 3; i++)
//  {
//    EPpulse(5,2);
//    EPpulse(0,10);
//  }
//
//  for(int j = 0; j < 3; j++)
//  {
//    EPpulse(-5,2);
//    EPpulse(0,10);
//  }
//
//  EPpulse(3,6);
//  EPpulse(0,50);
//
//  for(int k = 0; k < 4; k++)
//  {
//    EPpulse(2,50);
//    EPpulse(0,50);
//  }
//
//  EPpulse(-3,6);
//  EPpulse(0,50);
//
//  for(int m = 0; m < 4; m++)
//  {
//    EPpulse(-2,50);
//    EPpulse(0,50);
//  }
}

void softwareReset( uint8_t prescaller) {
  // start watchdog with the provided prescaller
  wdt_enable( prescaller);
  // wait for the prescaller time to expire
  // without sending the reset signal by using
  // the wdt_reset() method
  while(1) {}
}
