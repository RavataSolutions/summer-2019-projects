%Frequency sweep standard
plotType = 5;
    %0 - real vs imag
    %1 - freq vs imag
    %2 - freq vs real
    %3 - freq vs phase
    %4 - phase vs imag
    %5 - freq vs impedance
   
timeCol = 2;
freqCol = 1;
realCol = 3;
imagCol = 4;
phaseCol = 5; 
impedanceCol = 8;

data = C4_T1_2Cell;

[row,col] = size(data);
f = figure();
sgtitle('File:  Date:  Type:');
for(wellNum = 1:22)
    %get data
    time = data(1:row, timeCol);
    freqPlot = [4000:2000:104000];
    real = data(1:row, realCol + (6*(wellNum-1)));
    imag = data(1:row, imagCol + (6*(wellNum-1)));
    phase = data(1:row, phaseCol + (6*(wellNum-1)));
    impedance = data(1:row, impedanceCol + (6*(wellNum-1)));
    index = 1;
    count = 0;
    for(freq = 4000:2000:104000)
        realFreq(index) = mean(real(1+(20*count) : 20+(20*count),1));
        imagFreq(index) = mean(imag(1+(20*count) : 20+(20*count),1));
        phaseFreq(index) = rad2deg(mean(phase(1+(20*count) : 20+(20*count),1)));
        impedanceFreq(index) = mean(impedance(1+(20*count) : 20+(20*count),1));
        count = count+1;
        index = index+1;
    end
    
    subplot(5,5,wellNum);
    
    switch plotType
        case 0 %real vs imag
            plot(realFreq,imagFreq);
        case 1 %freq vs imag
            plot(freqPlot,imagFreq);
        case 2 %freq vs real
            plot(freqPlot,realFreq);
        case 3 %freq vs phase
            plot(freqPlot,phaseFreq);
        case 4 %phase vs imag
            plot(phaseFreq,imagFreq);
        case 5 %freq vs impedance
            plot(freqPlot,impedanceFreq);
    end
    title(wellNum);
end