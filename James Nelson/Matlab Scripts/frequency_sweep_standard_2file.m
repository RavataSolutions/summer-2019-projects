%Frequency sweep standard
plotType = 14;
    %0 - real vs imag
    %1 - freq vs imag
    %2 - freq vs real
    %3 - freq vs phase
    %4 - phase vs imag
    %5 - freq vs impedance
    %6 - phase vs real
    %7 - (Derivative) freq vs real
    %8 - (Derivative) freq vs imaginary
    %9 - (Derivative) freq vs Magnitude
    %10 - (Derivative) freq vs Phase
    %11 - (Difference) freq vs real
    %12 - (Difference) freq vs imaginary
    %13 - (Difference) freq vs Magnitude
    %14 - (Difference) freq vs Phase
timeCol = 2;
freqCol = 1;
realCol = 3;
imagCol = 4;
phaseCol = 5; 
impedanceCol = 8;

data = basline;%Blue
data2 = pre_EP;%Red
[row,col] = size(data);
f = figure();
sgtitle('Data:  Data2:  ExperimentDate:  plotType:');
for(wellNum = 1:22)
    %get data
    time = data(1:row, timeCol);
    freqPlot = [4000:2000:104000];
    real = data(1:row, realCol + (6*(wellNum-1)));
    imag = data(1:row, imagCol + (6*(wellNum-1)));
    phase = data(1:row, phaseCol + (6*(wellNum-1)));
    impedance = data(1:row, impedanceCol + (6*(wellNum-1)));
    real2 = data2(1:row, realCol + (6*(wellNum-1)));
    imag2 = data2(1:row, imagCol + (6*(wellNum-1)));
    phase2 = data2(1:row, phaseCol + (6*(wellNum-1)));
    impedance2 = data2(1:row, impedanceCol + (6*(wellNum-1)));
    
    index = 1;
    count = 0;
    for(freq = 4000:2000:104000)
        realFreq(index) = mean(real(1+(20*count) : 20+(20*count),1));
        imagFreq(index) = mean(imag(1+(20*count) : 20+(20*count),1));
        phaseFreq(index) = rad2deg(mean(phase(1+(20*count) : 20+(20*count),1)));
        impedanceFreq(index) = mean(impedance(1+(20*count) : 20+(20*count),1));
        
        realFreq2(index) = mean(real2(1+(20*count) : 20+(20*count),1));
        imagFreq2(index) = mean(imag2(1+(20*count) : 20+(20*count),1));
        phaseFreq2(index) = rad2deg(mean(phase2(1+(20*count) : 20+(20*count),1)));
        impedanceFreq2(index) = mean(impedance2(1+(20*count) : 20+(20*count),1));
        count = count+1;
        index = index+1;
    end
    
    subplot(5,5,wellNum);
    
    switch plotType
        case 0 %real vs imag
            plot(realFreq,imagFreq);
            hold on;
            plot(realFreq2,imagFreq2);
        case 1 %freq vs imag
            plot(freqPlot,imagFreq);
            hold on;
            plot(freqPlot,imagFreq2);
        case 2 %freq vs real
            plot(freqPlot,realFreq);
            hold on;
            plot(freqPlot,realFreq2);
        case 3 %freq vs phase
            plot(freqPlot,phaseFreq);
            hold on;
            plot(freqPlot,phaseFreq2);
        case 4 %phase vs imag
            plot(phaseFreq,imagFreq);
            hold on;
            plot(phaseFreq2,imagFreq2);
        case 5 %freq vs impedance
            plot(freqPlot,impedanceFreq);
            hold on;
            plot(freqPlot,impedanceFreq2);
        case 6 %phase vs real
            plot(phaseFreq,realFreq);
            hold on;
            plot(phaseFreq2,realFreq2);
        case 7 %Derivative freq vs real
            plot(freqPlot,gradient(realFreq)./gradient(freqPlot));
            hold on;
            plot(freqPlot,gradient(realFreq2)./gradient(freqPlot));
        case 8 %Derivative freq vs imaginary
            plot(freqPlot,gradient(imagFreq)./gradient(freqPlot));
            hold on;
            plot(freqPlot,gradient(imagFreq2)./gradient(freqPlot));
        case 9 %Derivative freq vs Magnitude
            plot(freqPlot,gradient(impedanceFreq)./gradient(freqPlot));
            hold on;
            plot(freqPlot,gradient(impedanceFreq2)./gradient(freqPlot));
        case 10 %Derivative freq vs Phase
            plot(freqPlot,gradient(phaseFreq)./gradient(freqPlot));
            hold on;
            plot(freqPlot,gradient(phaseFreq2)./gradient(freqPlot));
        case 11 %Difference freq vs real
            plot(freqPlot,realFreq-realFreq2);
        case 12 %Difference freq vs imaginary
            plot(freqPlot,imagFreq-imagFreq2);
        case 13 %Difference freq vs Magnitude
            plot(freqPlot,impedanceFreq-impedanceFreq2);
        case 14 %Difference freq vs Phase
            plot(freqPlot,phaseFreq-phaseFreq2);
    end
    if(wellNum == 1 && plotType <= 10)
        legend('Data','Data2');
    elseif(wellNum ==1 && plotType>=11 && plotType <=14)
        legend('Difference Data-data2');
    end
    hold off;
    title(wellNum);
end