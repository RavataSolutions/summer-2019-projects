import pandas as pd
from tkinter import filedialog
from tkinter import *
import bokeh.plotting as bp
import bokeh.models as bm
import bokeh.layouts as bl
from bokeh.models import HoverTool
from colorcet import glasbey
import bokeh

#html block
from bokeh.plotting import figure, output_file, show
from bokeh.util.browser import view
from bokeh.document import Document
from bokeh.embed import file_html
from bokeh.layouts import column, gridplot
from bokeh.models import  ColumnDataSource, Div, Grid, Line, LinearAxis, Plot, Range1d
from bokeh.resources import INLINE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class parent_well_dataframe():
    def __init__(self):
        self.spreadsheet = pd.DataFrame()
        self.time = pd.DataFrame()
        self.impedance_list = []
        self.phase_list = []
        self.system_phase_list = []
        self.imaginary_list = []
        self.real_list = []
        self.master_list = []
        self.plot_master_list = []
        self.master_grid = []
        self.filename = ""

    def openfile(self):
        # opens up file browser for data file
        root = Tk()
        root.filename = filedialog.askopenfilename(initialdir="/", title="Select file",
                                                   filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
        #filter filename into useable variable
        self.filename = str(root.filename)
        self.filename = self.filename.split(sep="/")
        self.filename = str(self.filename[-1])
        self.filename = self.filename.split(".")
        self.filename = self.filename[0:-1]
        self.filename = '.'.join(self.filename)

        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        return str(root.filename)

    def filterdata(self):
        file = self.openfile()
        # finds the first location of "TIME" in the data file and creates the dataframe from that
        spreadsheet = pd.read_csv(file)
        for i in range(len(spreadsheet)):
            list_check = list(spreadsheet.iloc[i])
            if (list_check[0] == "TIME"):
                spreadsheet = pd.read_csv(file, skiprows=(i + 1))
                break
        # masks the values above 10_000 ohms which correspond to air
        # masks them with blank NaN values to be excluded from graphing
        spreadsheet = spreadsheet.mask(spreadsheet > 10_000)
        return spreadsheet

    def initialize(self):
        self.spreadsheet = self.filterdata()
        self.time = pd.DataFrame(self.spreadsheet["TIME"])
        self.impedance_list = list(filter(lambda name: 'IMPEDANCE' in name, list(self.spreadsheet)))
        self.phase_list = list(filter(lambda name: name.startswith("PHASE"), list(self.spreadsheet)))
        self.system_phase_list = list(filter(lambda name: name.startswith("SYSTEM PHASE"), list(self.spreadsheet)))
        self.imaginary_list = list(filter(lambda name: 'IMAGINARY' in name, list(self.spreadsheet)))
        self.real_list = list(filter(lambda name: 'REAL' in name, list(self.spreadsheet)))
        for i in range(len(self.impedance_list)):
            if(len(self.impedance_list) == len(self.phase_list)):
                self.master_list.append([self.impedance_list[i], self.phase_list[i], self.real_list[i],
                                        self.imaginary_list[i], self.system_phase_list[i]])
#make a bunch of bokeh plots all at once on an html document
    def bokeh_plot(self):
        line_list = []
        for i in range(len(self.impedance_list)):
            line_list.append("line " + str(i))
        output_file("graph_all.html")
        line_dict = {"TIME": self.spreadsheet["TIME"].values}
        line_tuple_list = []
        legend = bokeh.models.Legend()
        line = Plot(output_backend="webgl")  # for the glyph API
        line = bp.figure(plot_width=1800, plot_height=700, title="Impedance Data Overlay",
                         x_axis_label="Time(s)", y_axis_label="Impedance(ohms)", output_backend="webgl")
        for i in range(len(self.impedance_list)):
            line_dict.update({self.impedance_list[i]: self.spreadsheet[self.impedance_list[i]].values})
            data_source = ColumnDataSource(line_dict)
            temp_list = []
            temp_list.append(line.step(x="TIME", y=self.impedance_list[i], line_color=glasbey[i], source=data_source,
                      line_width=2, muted_color=glasbey[i], muted_alpha=0.05, legend="Mute All Lines(not working correctly)"))
            line_list[i] = temp_list
            line_tuple = (self.impedance_list[i], line_list[i])
            line_tuple_list.append(line_tuple)
        #legend bullshit
        #legend = bokeh.models.Legend(items=line_tuple_list, location="center",click_policy ="hide")
        print(line_list[0])
        print(self.impedance_list[0])
        print(line_tuple_list[0])
        #legend = bokeh.models.Legend(items=[(self.impedance_list[0], line_list[0])], location="center", click_policy="hide")
        legend1 = bokeh.models.Legend(items=[(self.impedance_list[0], line_list[0]),
                                            (self.impedance_list[1], line_list[1]),
                                            (self.impedance_list[2], line_list[2]),
                                            (self.impedance_list[3], line_list[3]),
                                            (self.impedance_list[4], line_list[4]),
                                            (self.impedance_list[5], line_list[5]),
                                            (self.impedance_list[6], line_list[6]),
                                            (self.impedance_list[7], line_list[7]),
                                            (self.impedance_list[8], line_list[8]),
                                            (self.impedance_list[9], line_list[9]),
                                            (self.impedance_list[10], line_list[10]),
                                            (self.impedance_list[11], line_list[11]),
                                            (self.impedance_list[12], line_list[12]),
                                            (self.impedance_list[13], line_list[13])],
                                            location="center", click_policy="hide",
                                            orientation="horizontal")
        legend2 = bokeh.models.Legend(items=[(self.impedance_list[14], line_list[14]),
                                            (self.impedance_list[15], line_list[15]),
                                            (self.impedance_list[16], line_list[16]),
                                            (self.impedance_list[17], line_list[17]),
                                            (self.impedance_list[18], line_list[18]),
                                            (self.impedance_list[19], line_list[19]),
                                            (self.impedance_list[20], line_list[20]),
                                            (self.impedance_list[21], line_list[21]),
                                            (self.impedance_list[22], line_list[22]),
                                            (self.impedance_list[23], line_list[23]),
                                            (self.impedance_list[24], line_list[24]),
                                            (self.impedance_list[25], line_list[25]),
                                            (self.impedance_list[26], line_list[26]),
                                            (self.impedance_list[27], line_list[27])],
                                            location="center", click_policy="hide",
                                            orientation="horizontal")
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        line.add_layout(legend1, 'below')
        line.legend.location = "top_left"
        line.legend.click_policy = "mute"
        line.add_layout(legend2, 'below')
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        show(line)


parent = parent_well_dataframe()
parent.initialize()
parent.bokeh_plot()
