from tkinter import *
from tkinter.filedialog import askopenfilenames
import pandas as pd

class multifilesApp(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.csv_tuple = ()

    def initializeUI(self):
        self.master.title('Select Files Application')
        self.grid(row=0, column=0, sticky=W)

        # Create the button to select files
        self.button1 = Button(self.master, text='Select Files', command=self.selectFiles, width=10)
        self.button1.grid(row=30, column=0)

    def selectFiles(self):
        files = askopenfilenames(filetypes=(('CSV files', '*.csv'),
                                            ('All files', '*.*')),
                                 title='Select Input File'
                                 )
        self.csv_tuple = root.tk.splitlist(files)
        print('Your selected files = ', self.csv_tuple)

class single_dataframe:
  def __init__(self):
    self.filename = ""
    self.spreadsheet = pd.DataFrame()
    self.skipped_rows = 0
    self.file_path = ""

  def __filter_file_name(self, filename):
    # filter filename into useable variable
    self.filename = filename
    self.filename = self.filename.split(sep="/")
    self.file_path = self.filename[0:-1]
    self.file_path = "/".join(self.file_path)
    self.filename = str(self.filename[-1])
    self.filename = self.filename.split(".")
    self.filename = self.filename[0:-1]
    self.filename = '.'.join(self.filename)
    return self.filename
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def __read_csv(self, filename):
    print("reading", filename)
    dat = pd.read_csv(filename)
    for index, row in dat.iterrows():
      if (row[0] == "TIME"):
        self.skipped_rows = index + 1
        return pd.read_csv(filename, skiprows=self.skipped_rows)

  def __trim_impedance(self, dat):
    columns = list(filter(lambda x: "IMPEDANCE" in x, list(dat)))
    dat = dat.mask(dat[columns] > 10_000)
    for column in columns:
        #check if column has any valid values if not do nothing
        if(dat[column].first_valid_index() != None):
            dat[column] = dat[column].shift(-(dat[column].first_valid_index() + 1))
    return dat

  def process_csv(self, filename):
    dat = self.__read_csv(filename)
    dat = self.__trim_impedance(dat)
    dat.drop(dat.tail(1).index, inplace=True)
    self.file_name = self.__filter_file_name(filename)
    dat["DATASOURCE"] = self.file_name
    dat["REFERENCE"] = dat.index
    self.spreadsheet = dat
    return self

app = None
# loop file selection until user exits the tkinter window
if __name__ == "__main__":
    root = Tk()
    root.minsize(width=250, height=400)
    # root.geometry("1200x800")
    root.geometry("400x300")

    # Call the parser GUI application
    app = multifilesApp(master=root)
    app.initializeUI()
    app.mainloop()

dfs = [single_dataframe().process_csv(filename) for filename in app.csv_tuple]
dat = pd.concat([df.spreadsheet for df in dfs])
columns = list(filter(lambda x: "IMPEDANCE" in x, list(dat)))
dat = (dat[columns + ["DATASOURCE", "REFERENCE"]].melt(id_vars=['DATASOURCE', 'REFERENCE'], value_vars=columns, var_name='IMPEDANCE_TYPE', value_name='VALUE'))
dat.to_csv(r'C:\Users\matt\PycharmProjects\data_cleaner\impedance_clean_master_list.csv')