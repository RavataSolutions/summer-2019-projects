import pandas as pd
from tkinter import filedialog
from tkinter import *
import bokeh.plotting as bp
import bokeh.models as bm
import bokeh.layouts as bl
from bokeh.models import HoverTool
#open files gui dependencies
from tkinter import *
from tkinter.filedialog import askopenfilenames
#html block
from bokeh.util.browser import view
from bokeh.document import Document
from bokeh.embed import file_html
from bokeh.layouts import column, gridplot
from bokeh.models import  ColumnDataSource, Div, Grid, Line, LinearAxis, Plot, Range1d
from bokeh.resources import INLINE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class multifilesApp(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.csv_tuple = ()

    def initializeUI(self):
        self.master.title('Select Files Application')
        self.grid(row=0, column=0, sticky=W)

        # Create the button to select files
        self.button1 = Button(self.master, text='Select Files', command=self.selectFiles, width=10)
        self.button1.grid(row=30, column=0)

    def selectFiles(self):
        files = askopenfilenames(filetypes=(('CSV files', '*.csv'),
                                            ('All files', '*.*')),
                                 title='Select Input File'
                                 )
        self.csv_tuple = root.tk.splitlist(files)
        print('Your selected files = ', self.csv_tuple)

class single_dataframe:
  def __init__(self):
    self.filename = ""
    self.spreadsheet = pd.DataFrame()
    self.skipped_rows = 0
    self.file_path = ""

  def __filter_file_name(self, filename):
    # filter filename into useable variable
    self.filename = filename
    self.filename = self.filename.split(sep="/")
    self.file_path = self.filename[0:-1]
    self.file_path = "/".join(self.file_path)
    self.filename = str(self.filename[-1])
    self.filename = self.filename.split(".")
    self.filename = self.filename[0:-1]
    self.filename = '.'.join(self.filename)
    return self.filename
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def __read_csv(self, filename):
    dat = pd.read_csv(filename)
    for index, row in dat.iterrows():
      if (row[0] == "TIME"):
        self.skipped_rows = index + 1
        return pd.read_csv(filename, skiprows=self.skipped_rows)

  def __trim_impedance(self, dat): #ignore data over 10,000
    columns = list(filter(lambda x: "IMPEDANCE" in x, list(dat)))
    dat = dat.mask(dat[columns] > 10_000)
    for column in columns:
      dat[column] = dat[column].shift(-(dat[column].first_valid_index() + 1))
    return dat

  def process_csv(self, filename):  #run the data 
    dat = self.__read_csv(filename)
    dat = self.__trim_impedance(dat)
    dat.drop(dat.tail(1).index, inplace=True)
    self.file_name = self.__filter_file_name(filename)
    dat["DATASOURCE"] = self.file_name
    dat["REFERENCE"] = dat.index
    self.spreadsheet = dat
    return self

class parent_well_dataframe():
    def __init__(self):
        self.spreadsheet = pd.DataFrame()
        self.time = pd.DataFrame()
        self.impedance_list = []
        self.phase_list = []
        self.system_phase_list = []
        self.imaginary_list = []
        self.real_list = []
        self.master_list = []
        self.plot_master_list = []
        self.master_grid = []
        self.filename = ""

    def filterdata(self,filename):
        file = filename
        # finds the first location of "TIME" in the data file and creates the dataframe from that
        spreadsheet = pd.read_csv(file)
        for i in range(len(spreadsheet)):
            list_check = list(spreadsheet.iloc[i])
            if (list_check[0] == "TIME"):
                spreadsheet = pd.read_csv(file, skiprows=(i + 1))
                break

        spreadsheet.drop(spreadsheet.tail(1).index, inplace=True)
        # masks the values above 10_000 ohms which correspond to air
        # masks them with blank NaN values to be excluded from graphing
        spreadsheet.to_csv(r'C:\Users\matts\PycharmProjects\basic_graph_shawn\basic_graph_test.csv',
                           index=None, header=True)
        spreadsheet = spreadsheet.mask(spreadsheet > 10_000)
        return spreadsheet

    def initialize(self,filename):
        self.spreadsheet = self.filterdata(filename=filename)
        self.time = pd.DataFrame(self.spreadsheet["TIME"])
        self.impedance_list = list(filter(lambda name: 'IMPEDANCE' in name, list(self.spreadsheet)))
        self.phase_list = list(filter(lambda name: name.startswith("PHASE"), list(self.spreadsheet)))
        self.system_phase_list = list(filter(lambda name: name.startswith("SYSTEM PHASE"), list(self.spreadsheet)))
        self.imaginary_list = list(filter(lambda name: 'IMAGINARY' in name, list(self.spreadsheet)))
        self.real_list = list(filter(lambda name: 'REAL' in name, list(self.spreadsheet)))
        for i in range(len(self.impedance_list)):
            if(len(self.impedance_list) == len(self.phase_list)):
                self.master_list.append([self.impedance_list[i], self.phase_list[i], self.real_list[i],
                                        self.imaginary_list[i], self.system_phase_list[i]])
#make a bunch of bokeh plots all at once on an html document
    def bokeh_plot(self):
        grid = []
        # data_source = ColumnDataSource(self.spreadsheet)
        for i in range(len(self.impedance_list)):
            temp_list = []
            for j in range(len(self.master_list[i])):
                line_dict = {"x": self.spreadsheet["TIME"].values, "y": self.spreadsheet[self.master_list[i][j]].values, }
                data_source = ColumnDataSource(line_dict)
                line = bp.figure(plot_width=300, plot_height=300, title=self.master_list[i][j],
                                 x_axis_label="x", y_axis_label= "y")
                hover = HoverTool(tooltips=[("(x, y)", "(@x, @y)"), ], mode='vline')
                line.step(x="x", y="y", line_color="#231942",  legend=None, source=data_source, line_width=2)
                line.square(x="x", y="y", size=1, fill_alpha=0, line_alpha=0, angle=0.0, source=data_source)
                line.toolbar.active_inspect = None
                line.add_tools(hover)
                #graph background colors
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                line.outline_line_width = 7
                line.outline_line_alpha = 0.2
                line.outline_line_color = "#9593D9"
                line.background_fill_color = "#ECCFC3"
                line.background_fill_alpha = 0.2
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                temp_list.append(line)
            grid.append(temp_list)
        grid = bl.layout(children=grid)
        div = Div(text="""
        <h1>Ravata Impedance Data</h1>
        <p>This is Impedance Data generated by the Ravata Rize system
        </p>
        """)
        doc = Document()
        doc.add_root(column(div, grid, sizing_mode="scale_width"))
        if __name__ == "__main__":
            doc.validate()
            filename = "graph_all_" + str(self.filename) + ".html"
            with open(filename, "w") as f:
                f.write(file_html(doc, INLINE, "RAVATA"))
            print("Wrote %s" % filename)
            view(filename)


# declare app scope not in the if loop here
app = None
# loop file selection until user exits the tkinter window
if __name__ == "__main__":
    root = Tk()
    root.minsize(width=250, height=400)
    # root.geometry("1200x800")
    root.geometry("400x300")

    # Call the parser GUI application
    app = multifilesApp(master=root)
    app.initializeUI()
    app.mainloop()

for file in app.csv_tuple:
    parent = parent_well_dataframe()
    parent.initialize(file)
    parent.bokeh_plot()
